import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { DifficultyStoreModule } from './difficulty/difficulty.module';
import { EffectsModule } from '@ngrx/effects';
import { AuthStoreModule } from './auth/auth.module';
import { RouterStoreModule } from './router/router.module';
import { LobbyStoreModule } from './lobby';
import { SocketService } from '../common/services/socket.service';

@NgModule({
  imports: [
    StoreModule.forRoot({}),
    StoreDevtoolsModule.instrument({
      name: 'NgRx Guarding Athena',
      logOnly: environment.production,
    }),
    EffectsModule.forRoot(),

    RouterStoreModule,

    DifficultyStoreModule,
    AuthStoreModule,
    LobbyStoreModule,
  ],
  providers: [SocketService],
})
export class AppStoreModule {}
