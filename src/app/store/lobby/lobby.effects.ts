import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, filter, map, of, switchMap, withLatestFrom } from 'rxjs';
import { SocketService } from 'src/app/common/services';
import {
  AddLobby,
  DeleteLobby,
  JoinLobbyError,
  JoinLobbySuccess,
  LeaveLobbyError,
  RemoveUserLobby,
  RemoveUserLobbyError,
  StartLobby,
  UpdateLobby,
  UpdateRoleLobbyError,
} from './lobby.actions';
import { selectCurrentDifficulty } from '../difficulty';
import { CreateAndJoinLobby } from './lobby.actions';
import { selectCurrentLobbyId } from './lobby.selectors';
import {
  JoinLobby,
  LeaveLobby,
  LeaveLobbySuccess,
  selectIsLobbyOwner,
  UpdateRoleLobby,
} from '.';

@Injectable()
export class LobbyEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly socketService: SocketService,
  ) {}

  createAndJoinLobby$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CreateAndJoinLobby),
      withLatestFrom(
        this.store
          .select(selectCurrentDifficulty)
          .pipe(
            filter(
              (currentDifficulty): currentDifficulty is number =>
                !!currentDifficulty,
            ),
          ),
      ),
      switchMap(([{ lobby, role }, difficultyLevel]) =>
        this.socketService
          .createAndJoinLobby({ lobby: { ...lobby, difficultyLevel }, role })
          .pipe(catchError(() => of(JoinLobbyError()))),
      ),
    ),
  );

  joinLobby$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoinLobby),
      switchMap(({ lobbyId }) =>
        this.socketService
          .joinLobby(lobbyId)
          .pipe(catchError(() => of(JoinLobbyError()))),
      ),
    ),
  );

  leaveLobby$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LeaveLobby),
      withLatestFrom(this.store.select(selectCurrentLobbyId)),
      filter(([_, currentLobby]) => !!currentLobby),
      switchMap(() =>
        this.socketService
          .leaveLobby()
          .pipe(catchError(() => of(LeaveLobbyError()))),
      ),
    ),
  );

  startLobby$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StartLobby),
      withLatestFrom(
        this.store.select(selectCurrentLobbyId),
        this.store.select(selectIsLobbyOwner),
      ),
      filter(
        ([_, currentLobby, isLobbyOwner]) => !!currentLobby && isLobbyOwner,
      ),
      switchMap(() =>
        this.socketService
          .startLobby()
          .pipe(catchError(() => of(LeaveLobbyError()))),
      ),
    ),
  );

  updateRoleLobby$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UpdateRoleLobby),
      withLatestFrom(this.store.select(selectCurrentLobbyId)),
      filter(([_, currentLobby]) => !!currentLobby),
      switchMap(([{ role }]) =>
        this.socketService
          .updateRoleLobby(role)
          .pipe(catchError(() => of(UpdateRoleLobbyError()))),
      ),
    ),
  );

  removeUserLobby$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RemoveUserLobby),
      withLatestFrom(this.store.select(selectCurrentLobbyId)),
      filter(([_, currentLobby]) => !!currentLobby),
      switchMap(([{ userSteamId }]) =>
        this.socketService
          .removeUserLobby(userSteamId)
          .pipe(catchError(() => of(RemoveUserLobbyError()))),
      ),
    ),
  );

  lobbyLeaveSuccess$ = createEffect(() =>
    this.socketService.onLobbyLeaveSuccess$.pipe(
      map(() => LeaveLobbySuccess()),
    ),
  );

  lobbyJoinSuccess$ = createEffect(() =>
    this.socketService.onLobbyJoinSuccess$.pipe(
      map((lobby) => JoinLobbySuccess({ lobby })),
    ),
  );

  createLobbySync$ = createEffect(() =>
    this.socketService.onLobbyCreateSync$.pipe(
      map((lobby) => AddLobby({ lobby })),
    ),
  );

  updateLobbySync$ = createEffect(() =>
    this.socketService.onLobbyUpdateSync$.pipe(
      map((lobby) => UpdateLobby({ update: { id: lobby.id, changes: lobby } })),
    ),
  );

  deleteLobbySync$ = createEffect(() =>
    this.socketService.onLobbyDeleteSync$.pipe(
      map((id) => DeleteLobby({ id })),
    ),
  );
}
