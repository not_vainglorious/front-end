import { User } from '../auth/auth.type';
import { Difficulty } from '../difficulty';

export enum LobbyUserRole {
  UNKNOWN = 'UNKNOWN',
  CARRY = 'CARRY',
  MAP = 'MAP',
  ARC = 'ARC',
  PUGNA = 'PUGNA',
}

export enum LobbyRegion {
  'USWEST' = 1,
  'USEAST' = 2,
  'EUROPE' = 3,
  'KOREA' = 4,
  'SINGAPORE' = 5,
  'DUBAI' = 6,
  'AUSTRALIA' = 7,
  'STOCKHOLM' = 8,
  'AUSTRIA' = 9,
  'BRAZIL' = 10,
  'SOUTHAFRICA' = 11,
  'PWTELECOMSHANGHAI' = 12,
  'PWUNICOM' = 13,
  'CHILE' = 14,
  'PERU' = 15,
  'INDIA' = 16,
  'PWTELECOMGUANGZHOU' = 17,
  'PWTELECOMZHEJIANG' = 18,
  'JAPAN' = 19,
  'PWTELECOMWUHAN' = 20,
}

export type Lobby = {
  id: string;
  name: string;
  region: LobbyRegion;
  isStarted: boolean;
  password?: string;
  maxPlayers: number;
  difficultyLevel: Difficulty['level'];
  users: Array<LobbyUser>;
  createdAt: Date;
};

export type LobbyUser = {
  lobbyId: string;
  userSteamId: User['steamId'];
  role: LobbyUserRole;
  isOwner: boolean;
};

export type LobbyFull = Lobby & {
  users: Array<LobbyUser & Record<'user', User>>;
};
