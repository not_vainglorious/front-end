import { Update } from '@ngrx/entity';
import { createAction, props } from '@ngrx/store';
import { LobbyUser, LobbyUserRole } from '.';
import { Lobby, LobbyFull } from './lobby.type';

export enum LobbyActions {
  CREATE_AND_JOIN_LOBBY = '[Lobby] Create and join lobby',

  JOIN_LOBBY = '[Lobby] Join lobby',
  JOIN_LOBBY_SUCCESS = '[Lobby] Join lobby success',
  JOIN_LOBBY_ERROR = '[Lobby] Join lobby error',

  UPDATE_ROLE_LOBBY = '[Lobby] Update role',
  UPDATE_ROLE_LOBBY_ERROR = '[Lobby] Update role error',

  REMOVE_USER_LOBBY = '[Lobby] Remove user from lobby',
  REMOVE_USER_LOBBY_ERROR = '[Lobby] Remove user from lobby error',

  LEAVE_LOBBY = '[Lobby] Leave lobby',
  LEAVE_LOBBY_SUCCESS = '[Lobby] Leave lobby success',
  LEAVE_LOBBY_ERROR = '[Lobby] Leave lobby error',

  ADD_LOBBY = '[Lobby] Add lobby',
  UPDATE_LOBBY = '[Lobby] Update lobby',
  DELETE_LOBBY = '[Lobby] Delete lobby',

  START_LOBBY = '[Lobby] Start lobby',
  START_LOBBY_SUCCESS = '[Lobby] Start lobby success',

  SET_LOBBIES = '[Lobby] Set lobbies',
}

export const CreateAndJoinLobby = createAction(
  LobbyActions.CREATE_AND_JOIN_LOBBY,
  props<{ lobby: Pick<Lobby, 'name' | 'maxPlayers'>; role: LobbyUserRole }>(),
);

export const SetLobbies = createAction(
  LobbyActions.SET_LOBBIES,
  props<{ lobbies: Array<LobbyFull> }>(),
);

export const JoinLobby = createAction(
  LobbyActions.JOIN_LOBBY,
  props<Omit<LobbyUser, 'userSteamId' | 'isOwner' | 'role'>>(),
);

export const JoinLobbySuccess = createAction(
  LobbyActions.JOIN_LOBBY_SUCCESS,
  props<{ lobby: LobbyFull }>(),
);

export const JoinLobbyError = createAction(LobbyActions.JOIN_LOBBY_ERROR);

export const LeaveLobby = createAction(LobbyActions.LEAVE_LOBBY);

export const LeaveLobbySuccess = createAction(LobbyActions.LEAVE_LOBBY_SUCCESS);

export const LeaveLobbyError = createAction(LobbyActions.LEAVE_LOBBY_ERROR);

export const AddLobby = createAction(
  LobbyActions.ADD_LOBBY,
  props<{ lobby: LobbyFull }>(),
);

export const UpdateLobby = createAction(
  LobbyActions.UPDATE_LOBBY,
  props<{ update: Update<LobbyFull> }>(),
);

export const DeleteLobby = createAction(
  LobbyActions.DELETE_LOBBY,
  props<{ id: Lobby['id'] }>(),
);

export const UpdateRoleLobby = createAction(
  LobbyActions.UPDATE_ROLE_LOBBY,
  props<Pick<LobbyUser, 'role'>>(),
);

export const UpdateRoleLobbyError = createAction(
  LobbyActions.UPDATE_ROLE_LOBBY_ERROR,
);

export const RemoveUserLobby = createAction(
  LobbyActions.REMOVE_USER_LOBBY,
  props<Pick<LobbyUser, 'userSteamId'>>(),
);

export const RemoveUserLobbyError = createAction(
  LobbyActions.REMOVE_USER_LOBBY_ERROR,
);

export const StartLobby = createAction(LobbyActions.START_LOBBY);

export const StartLobbySuccess = createAction(
  LobbyActions.START_LOBBY_SUCCESS,
  props<Pick<Lobby, 'password'>>(),
);
