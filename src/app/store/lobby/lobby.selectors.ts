import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectAuthFeature } from '../auth/auth.selectors';
import { FEATURE_NAME } from './constants';
import { lobbyAdapter, LobbyState } from './lobby.reducer';

export const selectLobbyFeature =
  createFeatureSelector<LobbyState>(FEATURE_NAME);

const { selectIds, selectEntities, selectAll, selectTotal } =
  lobbyAdapter.getSelectors();

export const selectCurrentLobbyId = createSelector(
  selectLobbyFeature,
  ({ selectedLobby }) => selectedLobby,
);

export const selectLobbyLevels = createSelector(selectLobbyFeature, selectIds);

export const selectLobbyEntities = createSelector(
  selectLobbyFeature,
  selectEntities,
);

export const selectLobbies = createSelector(selectLobbyFeature, selectAll);

export const selectLobbyTotal = createSelector(selectLobbyFeature, selectTotal);

export const selectCurrentLobby = createSelector(
  selectCurrentLobbyId,
  selectLobbyFeature,
  (selectedLobby, { entities }) => entities[`${selectedLobby}`] || null,
);

export const selectAvailableLobbies = createSelector(
  selectLobbies,
  selectCurrentLobbyId,
  (lobbies, currentLobbyId) =>
    lobbies.filter(({ id, users, maxPlayers, isStarted }) => {
      const filter = users.length !== maxPlayers && !isStarted;

      if (currentLobbyId) {
        return filter && id !== currentLobbyId;
      }

      return filter;
    }),
);

export const selectIsLobbyOwner = createSelector(
  selectCurrentLobby,
  selectAuthFeature,
  (lobby, { user }) =>
    !!user &&
    !!user.steamId &&
    !!lobby &&
    !!lobby.users.find(
      (lobbyUser) =>
        lobbyUser.userSteamId === user.steamId && lobbyUser.isOwner,
    ),
);
