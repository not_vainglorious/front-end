import { createReducer, on } from '@ngrx/store';
import { Lobby, LobbyFull } from './lobby.type';
import * as LobbyActions from './lobby.actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export type LobbyState = {
  selectedLobby?: Lobby['id'];
  isLoading: boolean;
} & EntityState<LobbyFull>;

export const lobbyAdapter: EntityAdapter<LobbyFull> =
  createEntityAdapter<LobbyFull>({
    selectId: ({ id }) => id,
    sortComparer: (lobby1, lobby2) =>
      lobby1.createdAt === lobby2.createdAt
        ? 0
        : lobby1.createdAt > lobby2.createdAt
        ? -1
        : 1,
  });

const initialState = lobbyAdapter.getInitialState<{
  selectedLobby?: Lobby['id'];
  isLoading: boolean;
}>({
  isLoading: false,
});

export const lobbyReducer = createReducer(
  initialState,
  on(LobbyActions.SetLobbies, (state, { lobbies }) =>
    lobbyAdapter.upsertMany(lobbies, lobbyAdapter.removeAll(state)),
  ),
  on(LobbyActions.CreateAndJoinLobby, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(LobbyActions.JoinLobbySuccess, (state, { lobby }) => ({
    ...state,
    ...lobbyAdapter.upsertOne(lobby, state),
    selectedLobby: lobby.id,
    isLoading: false,
  })),
  on(LobbyActions.LeaveLobby, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(LobbyActions.LeaveLobbySuccess, (state) => ({
    ...state,
    isLoading: false,
    selectedLobby: initialState.selectedLobby,
  })),
  on(LobbyActions.AddLobby, (state, { lobby }) =>
    lobbyAdapter.addOne(lobby, state),
  ),
  on(LobbyActions.UpdateLobby, (state, { update }) =>
    lobbyAdapter.updateOne(update, state),
  ),
  on(LobbyActions.DeleteLobby, (state, { id }) => ({
    ...lobbyAdapter.removeOne(id, state),
    selectedLobby:
      id === state.selectedLobby
        ? initialState.selectedLobby
        : state.selectedLobby,
  })),
);
