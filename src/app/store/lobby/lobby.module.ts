import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FEATURE_NAME } from './constants';
import { LobbyEffects } from './lobby.effects';
import { lobbyReducer } from './lobby.reducer';

@NgModule({
  imports: [
    StoreModule.forFeature(FEATURE_NAME, lobbyReducer),
    EffectsModule.forFeature([LobbyEffects]),
  ],
})
export class LobbyStoreModule {}
