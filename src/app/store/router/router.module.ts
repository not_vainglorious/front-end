import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { routerReducer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { FEATURE_NAME } from './constants';

@NgModule({
  imports: [
    StoreModule.forFeature(FEATURE_NAME, routerReducer),
    StoreRouterConnectingModule.forRoot({ stateKey: FEATURE_NAME }),
  ],
})
export class RouterStoreModule {}
