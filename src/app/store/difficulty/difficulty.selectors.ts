import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FEATURE_NAME } from './constants';
import { difficultyAdapter, DifficultyState } from './difficulty.reducer';

export const selectDifficultyFeature =
  createFeatureSelector<DifficultyState>(FEATURE_NAME);

const { selectIds, selectEntities, selectAll, selectTotal } =
  difficultyAdapter.getSelectors();

export const selectCurrentDifficulty = createSelector(
  selectDifficultyFeature,
  ({ selectedDifficulty }) => selectedDifficulty,
);

export const selectDifficultyIsLoading = createSelector(
  selectDifficultyFeature,
  ({ isLoading }) => isLoading,
);

export const selectDifficultyLevels = createSelector(
  selectDifficultyFeature,
  selectIds,
);

export const selectDifficultyEntities = createSelector(
  selectDifficultyFeature,
  selectEntities,
);

export const selectDifficulties = createSelector(
  selectDifficultyFeature,
  selectAll,
);

export const selectDifficultyTotal = createSelector(
  selectDifficultyFeature,
  selectTotal,
);
