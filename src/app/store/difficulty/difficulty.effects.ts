import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  combineLatestWith,
  EMPTY,
  map,
  mergeMap,
  of,
  switchMap,
} from 'rxjs';
import { SocketService } from 'src/app/common/services';
import { UpdateDifficulties } from './difficulty.actions';
import { SetLobbies } from '../lobby';
import {
  ChangeDifficulty,
  ChangeDifficultyError,
  ChangeDifficultySuccess,
  LoadDifficulties,
  LoadDifficultiesError,
  LoadDifficultiesSuccess,
} from './difficulty.actions';
import { DifficultyService } from './services/difficulty.service';
import { Router } from '@angular/router';
import { Action, Store } from '@ngrx/store';
import { DifficultyActions } from '.';

@Injectable()
export class DifficultyEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly difficultyService: DifficultyService,
    private readonly socketService: SocketService,
    private readonly router: Router,
    private readonly store: Store,
  ) {}

  ngrxOnInitEffects(): Action {
    return { type: DifficultyActions.LOAD_DIFFICULTIES };
  }

  //
  // REST API
  //

  loadDifficulties$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoadDifficulties),
      switchMap(() =>
        this.difficultyService.getAll().pipe(
          map((difficulties) => LoadDifficultiesSuccess({ difficulties })),
          catchError(() => of(LoadDifficultiesError())),
        ),
      ),
    ),
  );

  //
  // WEBSOCKET OUTCOMING
  //

  changeDifficulty$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChangeDifficulty),
      combineLatestWith(this.socketService.onConnect$),
      switchMap(([{ level }]) =>
        this.socketService.changeDifficulty(level).pipe(
          catchError(() => {
            return of(ChangeDifficultyError());
          }),
        ),
      ),
    ),
  );

  //
  // WEBSOCKET INCOMING
  //

  /**
   * @todo Correct sync with two windows on init
   */
  selectedDifficultyChange$ = createEffect(() =>
    this.socketService.onDifficultyChangeSuccess$.pipe(
      mergeMap(({ difficultyLevel: level, lobbies }) => {
        return [ChangeDifficultySuccess({ level }), SetLobbies({ lobbies })];
      }),
    ),
  );

  difficultiesSync$ = createEffect(() =>
    this.socketService.onDifficultiesSync$.pipe(
      map((difficulties) =>
        UpdateDifficulties({
          updates: difficulties.map(({ level, ...changes }) => ({
            id: level,
            changes,
          })),
        }),
      ),
    ),
  );

  onDifficultySync$ = createEffect(
    () =>
      this.socketService.onDifficultySync$.pipe(
        map(({ difficultyLevel }) => {
          this.router.navigateByUrl(`dashboard/${difficultyLevel}`);
        }),
      ),
    { dispatch: false },
  );
}
