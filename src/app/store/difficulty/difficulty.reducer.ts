import { createReducer, on } from '@ngrx/store';
import { Difficulty } from './difficulty.type';
import * as DifficultyActions from './difficulty.actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export type DifficultyState = {
  selectedDifficulty?: Difficulty['level'];
  isLoading: boolean;
} & EntityState<Difficulty>;

export const difficultyAdapter: EntityAdapter<Difficulty> =
  createEntityAdapter<Difficulty>({
    selectId: ({ level }) => level,
  });

const initialState = difficultyAdapter.getInitialState<{
  selectedDifficulty?: Difficulty['level'];
  isLoading: boolean;
}>({
  isLoading: false,
});

export const difficultyReducer = createReducer(
  initialState,
  on(DifficultyActions.LoadDifficulties, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(DifficultyActions.LoadDifficultiesSuccess, (state, { difficulties }) => ({
    ...difficultyAdapter.setAll(difficulties, state),
    isLoading: false,
  })),
  on(DifficultyActions.LoadDifficultiesError, (state) => ({
    ...state,
    isLoading: false,
  })),
  on(DifficultyActions.ChangeDifficultySuccess, (state, { level }) => ({
    ...state,
    isLoading: false,
    selectedDifficulty: level,
  })),
  on(DifficultyActions.LoadDifficultiesError, (state) => ({
    ...state,
    isLoading: false,
  })),
  on(DifficultyActions.UpdateDifficulty, (state, { update }) => {
    return difficultyAdapter.updateOne(update, state);
  }),
  on(DifficultyActions.UpdateDifficulties, (state, { updates }) => {
    return difficultyAdapter.updateMany(updates, state);
  }),
);
