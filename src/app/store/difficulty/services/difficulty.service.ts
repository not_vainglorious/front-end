import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Difficulty } from '../difficulty.type';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DifficultyService {
  constructor(private readonly http: HttpClient) {}

  getAll(): Observable<Array<Difficulty>> {
    return this.http.get<Array<Difficulty>>('difficulties');
  }
}
