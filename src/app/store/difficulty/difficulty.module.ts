import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FEATURE_NAME } from './constants';
import { DifficultyEffects } from './difficulty.effects';
import { difficultyReducer } from './difficulty.reducer';
import { DifficultyService } from './services/difficulty.service';

@NgModule({
  imports: [
    StoreModule.forFeature(FEATURE_NAME, difficultyReducer),
    EffectsModule.forFeature([DifficultyEffects]),
  ],
  providers: [DifficultyService],
})
export class DifficultyStoreModule {}
