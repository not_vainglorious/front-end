export type Difficulty = {
  level: number;
  lobbiesCount: number;
};
