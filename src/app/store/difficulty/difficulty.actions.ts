import { Update } from '@ngrx/entity';
import { createAction, props } from '@ngrx/store';
import { Difficulty } from './difficulty.type';

export enum DifficultyActions {
  LOAD_DIFFICULTIES = '[Difficulty] Load difficulties',
  LOAD_DIFFICULTIES_SUCCESS = '[Difficulty] Load difficulties success',
  LOAD_DIFFICULTIES_ERROR = '[Difficulty] Load difficulties error',

  UPDATE_DIFFICULTIES = '[Difficulty] Update difficulties',

  CHANGE_DIFFICULTY = '[Difficulty] Change difficulty',
  CHANGE_DIFFICULTY_SUCCESS = '[Difficulty] Change difficulty success',
  CHANGE_DIFFICULTY_ERROR = '[Difficulty] Change difficulty error',

  UPDATE_DIFFICULTY = '[Difficulty] Update difficulty',
}

export const LoadDifficulties = createAction(
  DifficultyActions.LOAD_DIFFICULTIES,
);

export const LoadDifficultiesSuccess = createAction(
  DifficultyActions.LOAD_DIFFICULTIES_SUCCESS,
  props<{ difficulties: Array<Difficulty> }>(),
);

export const LoadDifficultiesError = createAction(
  DifficultyActions.LOAD_DIFFICULTIES_ERROR,
);

export const ChangeDifficulty = createAction(
  DifficultyActions.CHANGE_DIFFICULTY,
  props<{ level: Difficulty['level'] }>(),
);

export const ChangeDifficultySuccess = createAction(
  DifficultyActions.CHANGE_DIFFICULTY_SUCCESS,
  props<{ level: Difficulty['level'] }>(),
);

export const ChangeDifficultyError = createAction(
  DifficultyActions.CHANGE_DIFFICULTY_ERROR,
);

export const UpdateDifficulty = createAction(
  DifficultyActions.UPDATE_DIFFICULTY,
  props<{
    update: Update<Difficulty>;
  }>(),
);
export const UpdateDifficulties = createAction(
  DifficultyActions.UPDATE_DIFFICULTIES,
  props<{ updates: Update<Difficulty>[] }>(),
);
