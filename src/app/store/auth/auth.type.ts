export type User = {
  steamId: string;
  name: string;
  imageUrl: string;
  imageUrlMedium: string;
  imageUrlFull: string;
};
