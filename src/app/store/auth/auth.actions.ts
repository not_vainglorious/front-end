import { createAction, props } from '@ngrx/store';
import { User } from './auth.type';

export enum AuthActions {
  INIT = '[Auth] Init',

  SET_TOKEN = '[Auth] Set token',

  LOAD_USER = '[Auth] Load user',
  LOAD_USER_SUCCESS = '[Auth] Load user success',
  LOAD_USER_ERROR = '[Auth] Load user error',

  REDIRECT_STEAM_OAUTH = '[Auth] Redirect to Steam OAuth',
}

export const Init = createAction(AuthActions.INIT);

export const SetToken = createAction(
  AuthActions.SET_TOKEN,
  props<{ token: string }>(),
);

export const LoadUser = createAction(AuthActions.LOAD_USER);

export const LoadUserSuccess = createAction(
  AuthActions.LOAD_USER_SUCCESS,
  props<{ user: User }>(),
);

export const LoadUserError = createAction(AuthActions.LOAD_USER_ERROR);

export const RedirectSteamOAuth = createAction(
  AuthActions.REDIRECT_STEAM_OAUTH,
);
