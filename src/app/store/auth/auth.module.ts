import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FEATURE_NAME } from './constants';
import { AuthEffects } from './auth.effects';
import { authReducer } from './auth.reducer';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';

@NgModule({
  imports: [
    StoreModule.forFeature(FEATURE_NAME, authReducer),
    EffectsModule.forFeature([AuthEffects]),
  ],
  providers: [AuthService, UserService],
})
export class AuthStoreModule {}
