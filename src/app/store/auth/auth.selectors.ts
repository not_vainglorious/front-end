import { createFeatureSelector, createSelector, select } from '@ngrx/store';
import { FEATURE_NAME } from './constants';
import { AuthState } from './auth.reducer';
import { filter, map, pipe } from 'rxjs';
import { User } from './auth.type';

export const selectAuthFeature = createFeatureSelector<AuthState>(FEATURE_NAME);

export const selectToken = createSelector(
  selectAuthFeature,
  ({ token }) => token,
);

export const selectUser = pipe(
  select(selectAuthFeature),
  map(({ user }) => user),
  filter((user): user is User => !!user),
);
