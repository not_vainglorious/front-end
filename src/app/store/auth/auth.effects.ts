import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { catchError, EMPTY, map, of, switchMap } from 'rxjs';
import {
  AuthActions,
  Init,
  LoadUser,
  LoadUserError,
  LoadUserSuccess,
  RedirectSteamOAuth,
  SetToken,
} from './auth.actions';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';

@Injectable()
export class AuthEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly authService: AuthService,
    private readonly userService: UserService,

    private readonly router: Router,
  ) {}

  ngrxOnInitEffects(): Action {
    return { type: AuthActions.INIT };
  }

  init$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Init),
      switchMap(() => {
        const token = localStorage.getItem('token');

        if (token) {
          return [SetToken({ token })];
        }

        return EMPTY;
      }),
    ),
  );

  setToken$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SetToken),
      switchMap(() => of(LoadUser())),
    ),
  );

  loadUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoadUser),
      switchMap(() =>
        this.userService.getUser().pipe(
          map((user) => LoadUserSuccess({ user })),
          catchError(() => of(LoadUserError())),
        ),
      ),
    ),
  );

  redirectSteamOAuth$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(RedirectSteamOAuth),
        map(() => this.authService.redirectSteamOAuth()),
      ),
    { dispatch: false },
  );
}
