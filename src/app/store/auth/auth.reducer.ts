import { createReducer, on } from '@ngrx/store';
import { User } from './auth.type';
import * as AuthActions from './auth.actions';

export type AuthState = {
  user?: User;
  isLoading: boolean;
  token?: string;
};

export const initialState: AuthState = {
  isLoading: false,
};

export const authReducer = createReducer(
  initialState,
  on(AuthActions.LoadUser, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(AuthActions.SetToken, (state, { token }) => ({
    ...state,
    token,
  })),
  on(AuthActions.LoadUserSuccess, (state, { user }) => ({
    ...state,
    isLoading: false,
    user,
  })),
  on(AuthActions.LoadUserError, (state) => ({
    ...state,
    isLoading: false,
    token: initialState.token,
  })),
  on(AuthActions.RedirectSteamOAuth, (state) => ({
    ...state,
    isLoading: true,
  })),
);
