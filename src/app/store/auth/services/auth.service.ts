import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
  ) {}

  redirectSteamOAuth() {
    const { url, prefix } = environment;

    window.location.href = `${url}/${prefix}/auth/steam`;
  }
}
