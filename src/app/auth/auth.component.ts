import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { RedirectSteamOAuth } from '../store/auth/auth.actions';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent {
  constructor(private readonly store: Store) {}

  startAuth() {
    this.store.dispatch(RedirectSteamOAuth());
  }
}
