import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { filter, first, tap } from 'rxjs';
import { SetToken } from 'src/app/store/auth/auth.actions';
import { selectUser } from 'src/app/store/auth/auth.selectors';

@Component({
  template: '',
})
export class CallbackComponent implements OnInit {
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly store: Store,
  ) {}

  public ngOnInit(): void {
    const token = this.route.snapshot.queryParamMap.get('token');

    if (token) {
      localStorage.setItem('token', token);

      this.store
        .pipe(selectUser)
        .pipe(
          first(),
          tap(() => {
            this.router.navigateByUrl('dashboard');
          }),
        )
        .subscribe();

      this.store.dispatch(SetToken({ token }));

      return;
    }

    this.router.navigateByUrl('');
  }
}
