import { Directive, Input, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: 'img[fallback]',
})
export class FallbackImgDirective {
  @Input()
  @HostBinding('src')
  public src = '';

  @Input()
  public fallback = '';

  @HostListener('error')
  onError() {
    this.src = this.fallback;
  }
}
