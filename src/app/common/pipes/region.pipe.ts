import { Pipe, PipeTransform } from '@angular/core';
import { LobbyRegion } from 'src/app/store/lobby';

const RegionDictionary = Object.keys(LobbyRegion)
  .filter((key) => !Number.isInteger(+key))
  .reduce<Record<number, string>>(
    (acc, red) => ({
      ...acc,
      [LobbyRegion[red as keyof typeof LobbyRegion]]: red,
    }),
    {},
  );

@Pipe({ name: 'region' })
export class RegionPipe implements PipeTransform {
  transform(value: number): string {
    return RegionDictionary[value];
  }
}
