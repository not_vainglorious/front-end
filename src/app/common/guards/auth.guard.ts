import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';
import { selectAuthFeature } from 'src/app/store/auth/auth.selectors';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private readonly store: Store) {}

  public canActivate(): Observable<boolean> | boolean {
    return this.store.select(selectAuthFeature).pipe(
      filter(({ isLoading }) => !isLoading),
      first(),
      map(({ user }) => !!user),
    );
  }

  public canActivateChild(): Observable<boolean> | boolean {
    return this.canActivate();
  }
}
