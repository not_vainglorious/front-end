import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';
import { selectAuthFeature } from 'src/app/store/auth/auth.selectors';

@Injectable()
export class UnAuthGuard implements CanActivate, CanActivateChild {
  constructor(private readonly store: Store, private readonly router: Router) {}

  public canActivate(): Observable<boolean> | boolean {
    return this.store.select(selectAuthFeature).pipe(
      filter(({ isLoading }) => !isLoading),
      first(),
      map(({ user }) => {
        if (!!user) {
          this.router.navigate(['/dashboard']);
        }

        return !user;
      }),
    );
  }

  public canActivateChild(): Observable<boolean> | boolean {
    return this.canActivate();
  }
}
