import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { createSelector, select, Store } from '@ngrx/store';
import { filter, first, map, Observable } from 'rxjs';
import { ChangeDifficulty } from 'src/app/store/difficulty/difficulty.actions';
import { selectCurrentDifficulty } from 'src/app/store/difficulty/difficulty.selectors';
import { selectDifficulty } from 'src/app/store/router/router.selectors';

const selectDifficultyComparison = createSelector(
  selectCurrentDifficulty,
  selectDifficulty,
  (current, next) => ({
    isEven: current && next ? current === +next : false,
    next,
  }),
);

@Injectable()
export class DifficultyResolver implements Resolve<boolean> {
  constructor(private readonly store: Store) {}

  resolve(): Observable<boolean> {
    return this.store.pipe(
      select(selectDifficultyComparison),
      map(({ isEven, next }) => {
        if (!isEven && next) {
          this.store.dispatch(ChangeDifficulty({ level: +next }));
        }

        return isEven;
      }),
      filter((isEven) => isEven),
      map((isEven) => isEven),
      first(),
    );
  }
}
