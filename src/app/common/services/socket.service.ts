import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { EMPTY, filter, first, fromEvent, Observable, tap } from 'rxjs';
import { Difficulty } from 'src/app/store/difficulty/difficulty.type';
import { Lobby, LobbyFull, LobbyUser } from 'src/app/store/lobby';
import { selectAuthFeature } from 'src/app/store/auth/auth.selectors';
import { io, Socket } from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { SocketMessages } from './socket-messages.enum';

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  private socket: Socket;

  constructor(private readonly store: Store) {
    this.socket = io(`${environment.url}`, {
      autoConnect: false,
      transportOptions: {},
    });

    this.store
      .select(selectAuthFeature)
      .pipe(
        filter(({ isLoading, user }) => !isLoading && !!user),
        first(),
        tap(({ token }) => {
          this.socket.io.opts.transportOptions = {
            polling: {
              extraHeaders: {
                Authorization: 'Bearer ' + token,
              },
            },
          };

          this.socket.connect();
        }),
      )
      .subscribe();
  }

  /**
   * Sync Events
   */

  get onDifficultySync$(): Observable<
    Record<'difficultyLevel', Difficulty['level']>
  > {
    return fromEvent(this.socket, SocketMessages.DIFFICULTY_SYNC);
  }

  /**
   * Base Events
   */

  get onConnect$(): Observable<null> {
    return fromEvent<null>(this.socket, 'connect');
  }

  /**
   * Custom Events
   */

  get onDifficultiesSync$(): Observable<Array<Difficulty>> {
    return fromEvent(this.socket, SocketMessages.DIFFICULTIES_SYNC);
  }

  get onLobbyCreateSync$(): Observable<LobbyFull> {
    return fromEvent(this.socket, SocketMessages.LOBBY_CREATE_SYNC);
  }

  get onLobbyUpdateSync$(): Observable<LobbyFull> {
    return fromEvent(this.socket, SocketMessages.LOBBY_UPDATE_SYNC);
  }

  get onLobbyDeleteSync$(): Observable<Lobby['id']> {
    return fromEvent(this.socket, SocketMessages.LOBBY_DELETE_SYNC);
  }

  get onDifficultyChangeSuccess$(): Observable<
    Record<'difficultyLevel', Difficulty['level']> &
      Record<'lobbies', Array<LobbyFull>>
  > {
    return fromEvent(this.socket, SocketMessages.DIFFICULTY_CHANGE_SUCCESS);
  }

  get onLobbyLeaveSuccess$(): Observable<null> {
    return fromEvent<null>(this.socket, SocketMessages.LOBBY_LEAVE_SUCCESS);
  }

  get onLobbyJoinSuccess$(): Observable<LobbyFull> {
    return fromEvent(this.socket, SocketMessages.LOBBY_JOIN_SUCCESS);
  }

  /**
   * Emiters
   */

  changeDifficulty(level: Difficulty['level']): Observable<never> {
    this.socket.emit(SocketMessages.DIFFICULTY_CHANGE, level);

    return EMPTY;
  }

  createAndJoinLobby(createLobby: CreateLobby): Observable<never> {
    this.socket.emit(SocketMessages.LOBBY_CREATE_AND_JOIN, createLobby);

    return EMPTY;
  }

  joinLobby(id: Lobby['id']): Observable<never> {
    this.socket.emit(SocketMessages.LOBBY_JOIN, id);

    return EMPTY;
  }

  updateRoleLobby(role: LobbyUser['role']): Observable<never> {
    this.socket.emit(SocketMessages.LOBBY_ROLE_UPDATE, role);

    return EMPTY;
  }

  removeUserLobby(userSteamId: LobbyUser['userSteamId']): Observable<never> {
    this.socket.emit(SocketMessages.LOBBY_PLAYER_REMOVE, userSteamId);

    return EMPTY;
  }

  leaveLobby(): Observable<never> {
    this.socket.emit(SocketMessages.LOBBY_LEAVE);

    return EMPTY;
  }

  startLobby(): Observable<never> {
    this.socket.emit(SocketMessages.LOBBY_START);

    return EMPTY;
  }
}

type CreateLobby = Record<
  'lobby',
  Pick<Lobby, 'name' | 'difficultyLevel' | 'maxPlayers'>
> &
  Pick<LobbyUser, 'role'>;
