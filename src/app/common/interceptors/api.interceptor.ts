import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    if (req.headers.get('skip')) return next.handle(req);

    const { url, prefix } = environment;

    const apiReq = req.clone({
      url: `${url}/${prefix}/${req.url}`,
    });

    return next.handle(apiReq);
  }
}
