import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { ClipboardModule } from '@angular/cdk/clipboard';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';

import { TranslocoModule } from '@ngneat/transloco';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';

const modules = [
  CommonModule,
  FlexLayoutModule,
  RouterModule,

  TranslocoModule,

  MatSidenavModule,
  MatSelectModule,
  MatDividerModule,
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatButtonToggleModule,
  MatInputModule,
  MatExpansionModule,
  MatListModule,
  MatMenuModule,
  MatIconModule,
  MatTooltipModule,
  ClipboardModule,
];

@NgModule({
  imports: modules,
  exports: modules,
})
export class DashboardSharedModule {}
