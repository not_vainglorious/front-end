import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import {
  selectCurrentDifficulty,
  selectDifficulties,
} from 'src/app/store/difficulty/difficulty.selectors';
import { Difficulty } from 'src/app/store/difficulty/difficulty.type';

@Component({
  selector: 'app-difficulty-selector',
  templateUrl: './difficulty-selector.component.html',
})
export class DifficultySelectorComponent {
  public difficulties$: Observable<Array<Difficulty>>;
  public selectedDifficulty$: Observable<number>;

  constructor(private store: Store) {}

  ngOnInit() {
    this.difficulties$ = this.store.select(selectDifficulties);

    this.selectedDifficulty$ = this.store
      .select(selectCurrentDifficulty)
      .pipe(map((level) => +(level || 0)));
  }
}
