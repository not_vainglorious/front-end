import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-difficulty',
  templateUrl: './difficulty.component.html',
  styleUrls: ['./difficulty.component.scss'],
})
export class DifficultyComponent {
  @Input() level = 0;

  @Input() lobbiesCount = 0;

  @Input() active = false;

  public readonly DEFAULT_IMAGE = 'assets/difficulties/unknown.png';
}
