import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { selectCurrentDifficulty } from 'src/app/store/difficulty';

@Component({
  selector: 'app-difficulty-topbar',
  templateUrl: './difficulty-topbar.component.html',
})
export class DifficultyTopbarComponent {
  public difficultySelected$: Observable<boolean>;

  constructor(private readonly store: Store) {}

  ngOnInit() {
    this.difficultySelected$ = this.store
      .select(selectCurrentDifficulty)
      .pipe(map((difficulty) => !!difficulty));
  }
}
