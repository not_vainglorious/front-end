import { NgModule } from '@angular/core';

import { FallbackImgDirective } from 'src/app/common/directives';

import { DashboardSharedModule } from '../dashboard-shared.module';

import { DifficultySelectorComponent } from './difficulty-selector';
import { DifficultyComponent } from './difficulty';
import { NoSelectionComponent } from './no-selection';
import { DifficultyTopbarComponent } from './difficulty-topbar.component';

@NgModule({
  declarations: [
    DifficultySelectorComponent,
    DifficultyComponent,
    FallbackImgDirective,
    NoSelectionComponent,
    DifficultyTopbarComponent,
  ],
  imports: [DashboardSharedModule],
  bootstrap: [DifficultyComponent],
  exports: [DifficultyTopbarComponent],
})
export class DifficultyTopbarModule {}
