import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { DashboardSharedModule } from '../dashboard-shared.module';

import { LobbiesBoardComponent } from './lobbies-board.component';
import { LobbiesContainerComponent } from './lobbies-container';
import { CreateLobbyDialogComponent } from './create-lobby-dialog';
import { ActiveLobbyComponent } from './active-lobby';
import { NoLobbiesComponent } from './no-lobbies';
import { UsersListComponent } from './users-list';
import { LobbiesListComponent } from './lobbies-list';
import { RoleImageComponent } from './role-image';
import { RegionPipe } from 'src/app/common/pipes/region.pipe';

@NgModule({
  declarations: [
    LobbiesBoardComponent,
    LobbiesContainerComponent,
    CreateLobbyDialogComponent,
    ActiveLobbyComponent,
    LobbiesContainerComponent,
    LobbiesListComponent,
    NoLobbiesComponent,
    UsersListComponent,
    RoleImageComponent,

    RegionPipe,
  ],
  imports: [ReactiveFormsModule, DashboardSharedModule],
  bootstrap: [LobbiesBoardComponent],
  exports: [LobbiesBoardComponent],
})
export class LobbiesBoardModule {}
