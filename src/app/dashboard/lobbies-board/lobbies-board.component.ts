import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectDifficulties } from 'src/app/store/difficulty/difficulty.selectors';
import { Difficulty } from 'src/app/store/difficulty/difficulty.type';

@Component({
  selector: 'app-lobbies-board',
  templateUrl: './lobbies-board.component.html',
})
export class LobbiesBoardComponent {
  public difficulties$: Observable<Array<Difficulty>>;

  constructor(private readonly store: Store) {}

  ngOnInit() {
    this.difficulties$ = this.store.select(selectDifficulties);
  }
}
