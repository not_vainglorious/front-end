import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { filter, Observable } from 'rxjs';
import {
  CreateAndJoinLobby,
  JoinLobby,
  Lobby,
  LobbyFull,
  LobbyUser,
  selectAvailableLobbies,
  selectCurrentLobby,
} from 'src/app/store/lobby';
import { CreateLobbyDialogComponent } from '../create-lobby-dialog/create-lobby-dialog.component';

@Component({
  selector: 'app-lobbies-container',
  templateUrl: './lobbies-container.component.html',
  styleUrls: ['./lobbies-container.component.scss'],
})
export class LobbiesContainerComponent {
  public lobbies$: Observable<Array<LobbyFull>>;
  public lobby$: Observable<LobbyFull | null>;

  constructor(
    private readonly store: Store,
    private readonly dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.lobbies$ = this.store.select(selectAvailableLobbies);

    this.lobby$ = this.store.select(selectCurrentLobby);
  }

  openNewLobbyDialog() {
    const dialogRef = this.dialog.open<
      CreateLobbyDialogComponent,
      any,
      Pick<Lobby, 'name' | 'maxPlayers'> & Pick<LobbyUser, 'role'>
    >(CreateLobbyDialogComponent, {});

    dialogRef
      .afterClosed()
      .pipe(
        filter(
          (
            data,
          ): data is Pick<Lobby, 'name' | 'maxPlayers'> &
            Pick<LobbyUser, 'role'> => !!data,
        ),
      )
      .subscribe(({ role, ...lobby }) => {
        this.store.dispatch(CreateAndJoinLobby({ lobby, role }));
      });
  }

  joinLobby(id: Lobby['id']) {
    this.store.dispatch(JoinLobby({ lobbyId: id }));
  }
}
