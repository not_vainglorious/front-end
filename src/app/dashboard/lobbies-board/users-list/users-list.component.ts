import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from 'src/app/store/auth/auth.type';
import { LobbyUser, LobbyUserRole } from 'src/app/store/lobby';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent {
  roles = Object.keys(LobbyUserRole);

  @Input() users: Array<LobbyUser & Record<'user', User>> = [];

  @Input() userId?: User['steamId'];

  @Output() onUserRoleChange = new EventEmitter<LobbyUserRole>();

  @Output() onUserRemove = new EventEmitter<LobbyUser['userSteamId']>();

  get isLobbyOwner(): boolean {
    const lobbyOwner = this.users.find(({ isOwner }) => !!isOwner);

    return !!lobbyOwner && lobbyOwner.userSteamId === this.userId;
  }
}
