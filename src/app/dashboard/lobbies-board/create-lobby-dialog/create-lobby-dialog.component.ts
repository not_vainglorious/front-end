import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import {
  Lobby,
  LobbyRegion,
  LobbyUser,
  LobbyUserRole,
} from 'src/app/store/lobby/lobby.type';

const RegionValues = Object.keys(LobbyRegion).reduce<Array<number>>(
  (acc, key) => {
    if (Number.isInteger(+key)) {
      acc.push(+key);
    }
    return acc;
  },
  [],
);

const validateRegion = (c: FormControl) => {
  return RegionValues.includes(c.value)
    ? null
    : { validateRegion: { valid: false } };
};

const validateRole = (c: FormControl) => {
  return Object.keys(LobbyUserRole).includes(c.value)
    ? null
    : { validateRole: { valid: false } };
};

@Component({
  selector: 'app-create-lobby-dialog',
  templateUrl: './create-lobby-dialog.component.html',
  styleUrls: ['./create-lobby-dialog.component.scss'],
})
export class CreateLobbyDialogComponent {
  roles = Object.keys(LobbyUserRole);
  regions = RegionValues;

  validatorValues = {
    name: {
      minLength: 3,
      maxLength: 255,
    },
    maxPlayers: {
      min: 2,
      max: 4,
    },
  };

  createLobbyForm = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(this.validatorValues.name.minLength),
      Validators.maxLength(this.validatorValues.name.maxLength),
    ]),
    region: new FormControl(LobbyRegion.STOCKHOLM, [
      Validators.required,
      validateRegion,
    ]),
    maxPlayers: new FormControl(4, [
      Validators.required,
      Validators.min(this.validatorValues.maxPlayers.min),
      Validators.max(this.validatorValues.maxPlayers.max),
    ]),
    role: new FormControl(LobbyUserRole.UNKNOWN, [
      Validators.required,
      validateRole,
    ]),
  });

  constructor(
    private readonly dialogRef: MatDialogRef<
      CreateLobbyDialogComponent,
      Pick<Lobby, 'name' | 'maxPlayers'> & Pick<LobbyUser, 'role'>
    >,
  ) {}

  onSubmit() {
    if (this.createLobbyForm.valid) {
      this.dialogRef.close(this.createLobbyForm.value);

      return;
    }

    this.createLobbyForm.markAllAsTouched();
  }
}
