import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { selectUser } from 'src/app/store/auth/auth.selectors';
import { User } from 'src/app/store/auth/auth.type';
import {
  LeaveLobby,
  LobbyFull,
  LobbyUser,
  LobbyUserRole,
  RemoveUserLobby,
  selectCurrentLobby,
  selectIsLobbyOwner,
  StartLobby,
  UpdateRoleLobby,
} from 'src/app/store/lobby';

@Component({
  selector: 'app-active-lobby',
  templateUrl: './active-lobby.component.html',
  styleUrls: ['./active-lobby.component.scss'],
})
export class ActiveLobbyComponent implements OnInit {
  public lobby$: Observable<LobbyFull | null>;

  public userId$: Observable<User['steamId']>;

  public isOwner$: Observable<boolean>;

  constructor(private readonly store: Store) {}

  ngOnInit() {
    this.lobby$ = this.store.select(selectCurrentLobby);

    this.userId$ = this.store
      .pipe(selectUser)
      .pipe(map(({ steamId }) => steamId));

    this.isOwner$ = this.store.select(selectIsLobbyOwner);
  }

  leaveLobby() {
    this.store.dispatch(LeaveLobby());
  }

  startLobby() {
    this.store.dispatch(StartLobby());
  }

  roleLobbyChange(role: LobbyUserRole) {
    this.store.dispatch(UpdateRoleLobby({ role }));
  }

  userLobbyRemove(userSteamId: LobbyUser['userSteamId']) {
    this.store.dispatch(RemoveUserLobby({ userSteamId }));
  }
}
