import { Component, Input } from '@angular/core';
import { LobbyUserRole } from 'src/app/store/lobby';

@Component({
  selector: 'app-role-image',
  templateUrl: './role-image.component.html',
  styleUrls: ['./role-image.component.scss'],
})
export class RoleImageComponent {
  @Input() role: string = LobbyUserRole.UNKNOWN;
}
