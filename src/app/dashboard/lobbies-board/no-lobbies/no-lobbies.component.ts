import { Component } from '@angular/core';

@Component({
  selector: 'app-no-lobbies',
  templateUrl: './no-lobbies.component.html',
  styleUrls: ['./no-lobbies.component.scss'],
})
export class NoLobbiesComponent {}
