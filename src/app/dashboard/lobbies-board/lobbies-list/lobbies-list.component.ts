import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Lobby, LobbyFull } from 'src/app/store/lobby';

@Component({
  selector: 'app-lobbies-list',
  templateUrl: './lobbies-list.component.html',
  styleUrls: ['./lobbies-list.component.scss'],
})
export class LobbiesListComponent {
  @Input() lobbies: Array<LobbyFull> = [];

  @Output() onLobbyJoin = new EventEmitter<Lobby['id']>();
}
