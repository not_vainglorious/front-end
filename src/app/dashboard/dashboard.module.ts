import { NgModule } from '@angular/core';

import { TRANSLOCO_SCOPE } from '@ngneat/transloco';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardSharedModule } from './dashboard-shared.module';

import { DifficultyTopbarModule } from './difficulty-topbar';
import { LobbiesBoardModule } from './lobbies-board';

import { DashboardComponent } from './dashboard.component';
import { SettingsComponent } from './settings';

@NgModule({
  declarations: [DashboardComponent, SettingsComponent],
  providers: [{ provide: TRANSLOCO_SCOPE, useValue: 'dashboard' }],
  imports: [
    DashboardRoutingModule,
    DashboardSharedModule,

    DifficultyTopbarModule,
    LobbiesBoardModule,
  ],
})
export class DashboardModule {}
