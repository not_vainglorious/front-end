import { Component } from '@angular/core';
import { LangDefinition, TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent {
  constructor(public translocoService: TranslocoService) {}

  get languages(): Array<LangDefinition> {
    return this.translocoService.getAvailableLangs() as Array<LangDefinition>;
  }

  get activeLanguage(): string {
    return this.translocoService.getActiveLang();
  }
}
