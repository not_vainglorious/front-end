import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DifficultyResolver } from '../common/resolvers/difficulty.resolver';
import { DashboardComponent } from './dashboard.component';
import { LobbiesBoardComponent } from './lobbies-board/lobbies-board.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: ':level',
        resolve: { difficultyLoaded: DifficultyResolver },
        component: LobbiesBoardComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [DifficultyResolver],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
