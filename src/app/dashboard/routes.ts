import { Routes } from '@angular/router';
import { AuthGuard } from '../common/guards/auth.guard';
import { DashboardComponent } from './dashboard.component';

export const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
];
